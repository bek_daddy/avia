  Drop table Users
  Drop table Companies

  Drop table PayTickets
  Drop table RefundTickets
  Drop table CreditTickets

  CREATE TABLE Users
(
  [Id] int  IDENTITY(1,1),
  [LastName] nvarchar(64) not null,
  [FirstName] nvarchar(64) not null,
  [FatherName] nvarchar(64) null,
  [Address] nvarchar(255) not null,
  [NumberPhone] nvarchar(25) null,
  [Role] nvarchar(15) not null,
  [Salary] money not null,
  [DateFirst] date not null,
  [Login] nvarchar(32),
  [Password] nvarchar(32),
  [IsDelete] bit Default(0)
  )


  CREATE TABLE Companies
  (
  [Id] int IDENTITY(1,1),
  [Code] nvarchar(15) not null,
  [Name] nvarchar(64) not null,
  [Interest] nvarchar(max) not null,
  [IsDelete] bit Default(0)
  )

  CREATE TABLE PayTickets
  (
  [Id] int IDENTITY(1,1),
  [FIO] nvarchar(128) not null,
  [Direction] nvarchar(128),
  [Reis] nvarchar(32),
  [IsOne] bit,
  [ValutTarif] money,
  [Rate] money,
  [SomTarif] money,
  [TopSbor] money,
  [AeroportSbor] money,
  [ItogoPoBiletu] money,
  [Comission] money,
  [ComissionSom] money,
  [Skidka] money,
  [OplataPassajir] money,
  [Itogo] money,
  [IdCompany] int,
  [IdUser] int,
  [IsReport] bit Default(0),
  [IsDelete] bit Default(0),
  [IsCredit] bit Default(0),
  [DopCommission] money
  )
 
  CREATE TABLE RefundTickets
  (
  [Id] int IDENTITY(1,1),
  [FIO] nvarchar(128) not null,
  [Direction] nvarchar(128),
  [Reis] nvarchar(32),
  [IsOne] bit,
  [ValutTarif] money,
  [Rate] money,
  [SomTarif] money,
  [TopSbor] money,
  [AeroportSbor] money,
  [ItogoPoBiletu] money,
  [Comission] money,
  [ComissionSom] money,
  [Skidka] money,
  [OplataPassajir] money,
  [Itogo] money,
  [IdCompany] int,
  [IdUser] int,
  [IsReport] bit Default(0),
  [IsDelete] bit Default(0),
  [IsCredit] bit Default(0),
  [DopCommission] money
  )

  CREATE TABLE CreditTickets
  (
  [Id] int IDENTITY(1,1),
  [IdTicket] int,
  [PaidValue] money,
  [CreditValue] money
  )

  Insert into Users  (
      [LastName]
      ,[FirstName]
      ,[FatherName]
      ,[Address]
      ,[NumberPhone]
      ,[Role]
      ,[Salary]
      ,[DateFirst]
      ,[Login]
      ,[Password]
      )  Values
	  (
	  'admin',
	  'admin',
	  'admin',
	  'address',
	  '0700',
	  'admin',
	  0,
	  GETDATE(),
	  'admin',
	  'admin'
	  )

  Insert into Companies  (
       [Code]
      ,[Name]
      ,[Interest]
      ,[IsDelete]
      )  Values
	  (
	  'Aa',
	  'BiletKG',
	  '7',
	   0
	  )

