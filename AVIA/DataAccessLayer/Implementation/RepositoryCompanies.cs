﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DataAccessLayer.Interface;
using Domain.Entities;

namespace DataAccessLayer.Implementation
{
    public class RepositoryCompanies : IRepositoryCompanies
    {
        public SqlTransaction Transaction { get; set; }

        private readonly SqlConnection _connection;
        public RepositoryCompanies()
        {
            _connection = new SqlConnection(DBSettings.Default.DbConnect);
        }

        public IEnumerable<Company> GetAllCompanies()
        {
            try
            {
                return _connection.Query<Company>("Select * from dbo.Companies");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Company> GetNotDeletedCompanies()
        {
            try
            {
                return _connection.Query<Company>("Select * from dbo.Companies Where IsDelete =0");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Company GetCompanyById(int id)
        {
            try
            {
                return
                    _connection.Query<Company>("Select * from dbo.Companies Where Id = @id", new { @id = id })
                               .FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Company Save(Company company)
        {
            try
            {
                int id = 0;
                if (company.Id != 0)
                    id = AddNewCompanies(company);
                else
                {
                    id = company.Id;
                    _connection.Execute(@"Update Companies Set Code=  @code,
                                                               Name= @name,
                                                               Interest = @interest", new
                                                                                    {
                                                                                        @code = company.Code,
                                                                                        @name = company.Name,
                                                                                        @interest = company.Interest
                                                                                    });
                }

                return GetCompanyById(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void DeleteCompanies(Company company)
        {
            try
            {
                _connection.Execute("Update Companies Set IsDelete =1 Where Id =@id", new { @id = company.Id });
            }
            catch (Exception)
            {

            }
        }

        private int AddNewCompanies(Company company)
        {
            try
            {
                var reader = _connection.ExecuteReader(@"Insert into Companies Values
                                                               ( 
                                                                @code,
                                                                @name,
                                                                @interest
                                                                ) 
                                                                SCOPE_IDENTITY()", new
                       {
                           @code = company.Code,
                           @name = company.Name,
                           @interest = company.Interest
                       });

                var id = "";
                while (reader.Read())
                {
                    id = reader.GetValue(0).ToString();
                }
                return Convert.ToInt32(id);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
