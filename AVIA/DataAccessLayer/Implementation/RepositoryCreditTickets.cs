﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DataAccessLayer.Interface;
using Domain.Entities;

namespace DataAccessLayer.Implementation
{
    public class RepositoryCreditTickets : IRepositoryCreditTickets
    {
        private readonly SqlConnection _sqlConnection;

        public RepositoryCreditTickets()
        {
            _sqlConnection = new SqlConnection(DBSettings.Default.DbConnect);
        }

        public IEnumerable<CreditTicket> GetAll()
        {
            try
            {
                

                return _sqlConnection.Query<CreditTicket>("Select * from CreditTickets");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<CreditTicket> GetByIdTicket(int idTicket)
        {
            try
            {


                return _sqlConnection.Query<CreditTicket>("Select * from dbo.CreditTickets Where IdTicket = @id_ticket",
                                                     new { @id_ticket = idTicket });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public CreditTicket GetCreditTicketById(int id)
        {
            try
            {
                 

                return
                    _sqlConnection.Query<CreditTicket>("Select * from CreditTickets Where id = @id", new { @id = id })
                                  .FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Save(CreditTicket creditTicket)
        {
            try
            {


                _sqlConnection.Execute("Insert into CreditTickets VALUES ( @id_ticket, @credit_value, @paid_value)",
                                       new
                                           {
                                               @id_ticket = creditTicket.IdTicket,
                                               @credit_value = creditTicket.CreditValue,
                                               @paid_value = creditTicket.PaidValue
                                           });
            }
            catch (Exception)
            {
                
            }

        }

        public void Delete(CreditTicket creditTicket)
        {
            try
            {
                _sqlConnection.Execute("Delete from CreditTickets Where Id = @id", new {creditTicket.Id});
            }
            catch (Exception)
            {
                 
            }
        }
    }
}
