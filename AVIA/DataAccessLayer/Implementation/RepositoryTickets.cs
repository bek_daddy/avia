﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DataAccessLayer.Interface;
using Domain.Entities;

namespace DataAccessLayer.Implementation
{
    public class RepositoryTickets : IRepositoryTickets
    {
        private readonly SqlConnection _sqlConnection;
        public RepositoryTickets()
        {
            _sqlConnection = new SqlConnection(DBSettings.Default.DbConnect);
        }

        public IEnumerable<Ticket> GetAllPaidTickets()
        {
            try
            {
                return _sqlConnection.Query<PaidTicket, Company, PaidTicket>("Select * from dbo.PayTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany",
                                                                             (ticket, company) =>
                                                                             {
                                                                                 ticket.Company = company;
                                                                                 return ticket;
                                                                             });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetAllRefundTieckets()
        {
            try
            {
                return _sqlConnection.Query<RefundTicket, Company, RefundTicket>("Select * from dbo.RefundTickets refund  LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany",
                                                                             (ticket, company) =>
                                                                             {
                                                                                 ticket.Company = company;
                                                                                 return ticket;
                                                                             });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetAllPaidTicketsByUser(User user)
        {
            try
            {
                return _sqlConnection.Query<PaidTicket, Company, PaidTicket>("Select * from dbo.PayTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IdUser = @idUser",
                                                            (ticket, company) =>
                                                            {
                                                                ticket.Company = company;
                                                                return ticket;
                                                            }, new { @idUser = user.Id });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetAllRefundTicketsByUser(User user)
        {
            try
            {
                return _sqlConnection.Query<RefundTicket, Company, RefundTicket>("Select * from dbo.RefundTickets refund  LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IdUser = @idUser",
                     (ticket, company) =>
                     {
                         ticket.Company = company;
                         return ticket;
                     }, new { @idUser = user.Id });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetNotDeletedPaidTicketsByUser(User user)
        {
            try
            {
                return _sqlConnection.Query<PaidTicket, Company, PaidTicket>("Select * from dbo.PayTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IdUser = @idUser AND IsDelete =0",
                    (ticket, company) =>
                    {
                        ticket.Company = company;
                        return ticket;
                    }, new { @idUser = user.Id });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetNotDeletedRefundTicketsByUser(User user)
        {
            try
            {
                return _sqlConnection.Query<RefundTicket, Company, RefundTicket>("Select * from dbo.RefundTickets refund  LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IdUser = @idUser AND IsDelete =0",
                                                       (ticket, company) =>
                                                       {
                                                           ticket.Company = company;
                                                           return ticket;
                                                       }, new { @idUser = user.Id });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetNotDeletedPaidTickets()
        {
            try
            {
                return _sqlConnection.Query<PaidTicket, Company, PaidTicket>("Select * from dbo.PayTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IsDelete = 0",
                (ticket, company) =>
                {
                    ticket.Company = company;
                    return ticket;
                });
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<Ticket> GetNotDeletedRefundTickets()
        {
            try
            {
                return _sqlConnection.Query<RefundTicket, Company, RefundTicket>("Select * from dbo.RefundTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where IsDelete =0",
                     (ticket, company) =>
                     {
                         ticket.Company = company;
                         return ticket;
                     });
            }
            catch (Exception)
            {
                return null;
            }
        }


        public Ticket GetByIdPaidTicket(int id)
        {
            try
            {
                return
                    _sqlConnection.Query<PaidTicket, Company, PaidTicket>("Select * from dbo.PayTickets paid LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where Id =@id ",
                    (ticket, company) =>
                    {
                        ticket.Company = company;
                        return ticket;
                    }, new { @id = id }).FirstOrDefault(); ;

            }
            catch (Exception)
            {
                return null;
            }
        }

        public Ticket GetByIdRefundTicket(int id)
        {
            try
            {
                return
                    _sqlConnection.Query<RefundTicket, Company, RefundTicket>("Select * from dbo.RefundTickets refund  LEFT  JOIN dbo.Companies c ON c.Id = paid.IdCompany Where Id =@id ",
                      (ticket, company) =>
                      {
                          ticket.Company = company;
                          return ticket;
                      }, new { @id = id }).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Ticket Save(Ticket ticket)
        {
            try
            {
                if (ticket.GetType() == typeof(PaidTicket))
                {
                    if (ticket.Id == 0)
                    {
                        var id = InsertNewPayTicket(ticket);
                        return GetByIdPaidTicket(id);
                    }

                    _sqlConnection.Execute(@"Update PayTickets Set 
                                                                                        @fio,
                                                                                        @direaction, 
                                                                                        @reis,
                                                                                        @valut_tarif,
                                                                                        @rate,
                                                                                        @top_sbor,
                                                                                        @aeroport_sbor,
                                                                                        @itogo_po_biletu,
                                                                                        @comm,
                                                                                        @comm_som,
                                                                                        @skidka,
                                                                                        @oplata_passajir,
                                                                                        @itogo,
                                                                                        @id_company,
                                                                                        @id_user 
                                                                                        @IsReport,
                                                                                        @IsCredit, 
                                                                                        @IsDelete", new
                                                                                                           {
                                                                                                               @fio = ticket.FIO,
                                                                                                               @direaction = ticket.Direction,
                                                                                                               @reis = ticket.Reis,
                                                                                                               @valut_tarif = ticket.ValutTarif,
                                                                                                               @rate = ticket.Rate,
                                                                                                               @top_sbor = ticket.TopSbor,
                                                                                                               @aeroport_sbor = ticket.AeroportSbor,
                                                                                                               @itogo_po_biletu = ticket.ItogoPoBiletu,
                                                                                                               @comm = ticket.Comission,
                                                                                                               @comm_som = ticket.ComissionSom,
                                                                                                               @skidka = ticket.Skidka,
                                                                                                               @oplata_passajir = ticket.OplataPassajir,
                                                                                                               @itogo = ticket.Itogo,
                                                                                                               @id_company = ticket.Company.Id,
                                                                                                               @id_user = ticket.User.Id,
                                                                                                               @IsReport = ticket.IsReport,
                                                                                                               @IsCredit = ticket.IsCredit,
                                                                                                               @IsDelete = ticket.IsDelete
                                                                                                           });
                    return GetByIdPaidTicket(ticket.Id);
                }




                var reader = _sqlConnection.ExecuteReader(@"Insert into RefundTickets Values (
                                                                                        @fio,
                                                                                        @direaction, 
                                                                                        @reis,
                                                                                        @valut_tarif,
                                                                                        @rate,
                                                                                        @top_sbor,
                                                                                        @aeroport_sbor,
                                                                                        @itogo_po_biletu,
                                                                                        @comm,
                                                                                        @comm_som,
                                                                                        @skidka,
                                                                                        @oplata_passajir,
                                                                                        @itogo,
                                                                                        @id_company,
                                                                                        @id_user
                                                                                        ) SCOPE_IDENTITY()", new
                                                                                                           {
                                                                                                               @fio = ticket.FIO,
                                                                                                               @direaction = ticket.Direction,
                                                                                                               @reis = ticket.Reis,
                                                                                                               @valut_tarif = ticket.ValutTarif,
                                                                                                               @rate = ticket.Rate,
                                                                                                               @top_sbor = ticket.TopSbor,
                                                                                                               @aeroport_sbor = ticket.AeroportSbor,
                                                                                                               @itogo_po_biletu = ticket.ItogoPoBiletu,
                                                                                                               @comm = ticket.Comission,
                                                                                                               @comm_som = ticket.ComissionSom,
                                                                                                               @skidka = ticket.Skidka,
                                                                                                               @oplata_passajir = ticket.OplataPassajir,
                                                                                                               @itogo = ticket.Itogo,
                                                                                                               @id_company = ticket.Company.Id,
                                                                                                               @id_user = ticket.User.Id
                                                                                                           });
                var idd = "";
                while (reader.Read())
                {
                    idd = reader.GetValue(0).ToString();
                }
                return GetByIdRefundTicket(Convert.ToInt32(idd));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool DeletePaidTicket(Ticket ticket)
        {
            try
            {
                return _sqlConnection.Execute("Update PayTickets Set IsDelete = 1 Where Id =@id", new { @id = ticket.Id }) == 1;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool DeleteRefundedTicket(Ticket ticket)
        {
            try
            {
                return _sqlConnection.Execute("Update RefundTickets Set IsDelete = 1 Where Id =@id", new { @id = ticket.Id }) == 1;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private int InsertNewPayTicket(Ticket ticket)
        {
            var reader = _sqlConnection.ExecuteReader(@"Insert into PayTickets Values (
                                                                                        @fio,
                                                                                        @direaction, 
                                                                                        @reis,
                                                                                        @valut_tarif,
                                                                                        @rate,
                                                                                        @top_sbor,
                                                                                        @aeroport_sbor,
                                                                                        @itogo_po_biletu,
                                                                                        @comm,
                                                                                        @comm_som,
                                                                                        @skidka,
                                                                                        @oplata_passajir,
                                                                                        @itogo,
                                                                                        @id_company,
                                                                                        @id_user
                                                                                        ) SCOPE_IDENTITY()", new
                {
                    @fio = ticket.FIO,
                    @direaction = ticket.Direction,
                    @reis = ticket.Reis,
                    @valut_tarif = ticket.ValutTarif,
                    @rate = ticket.Rate,
                    @top_sbor = ticket.TopSbor,
                    @aeroport_sbor = ticket.AeroportSbor,
                    @itogo_po_biletu = ticket.ItogoPoBiletu,
                    @comm = ticket.Comission,
                    @comm_som = ticket.ComissionSom,
                    @skidka = ticket.Skidka,
                    @oplata_passajir = ticket.OplataPassajir,
                    @itogo = ticket.Itogo,
                    @id_company = ticket.Company.Id,
                    @id_user = ticket.User.Id
                });
            var id = "";
            while (reader.Read())
            {
                id = reader.GetValue(0).ToString();
            }
            return Convert.ToInt32(id);
        }

    }
}
