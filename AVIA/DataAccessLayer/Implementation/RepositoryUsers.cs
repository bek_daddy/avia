﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataAccessLayer.Interface;
using Domain.Entities;
using Dapper;

namespace DataAccessLayer.Implementation
{
    public class RepositoryUsers : IRepositoryUsers
    {
        private readonly SqlConnection _connection;
        public RepositoryUsers()
        {
            _connection = new SqlConnection(DBSettings.Default.DbConnect);
        }

        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                return _connection.Query<User>("Select * from dbo.Users");
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<User> GetNotDeleteUsers()
        {
            try
            {
                return _connection.Query<User>("Select * from dbo.Users Where IsDelete =0");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public User GetUserByLoginPassword(string login, string password)
        {
            try
            {
                return _connection.Query<User>("Select * from dbo.Users Where Login = @log AND Password =@pass AND IsDelete =0", new
                                {
                                    @log = login,
                                    @pass = password
                                }).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public User GetUserById(int id)
        {
            try
            {
                return _connection.Query<User>("Select * from dbo.Users Where Id = @id",
                    new { @id = id }).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool DeleteUser(User user)
        {
            try
            {
                return _connection.Execute("Update Users Set IsDelete = 1 Where Id =@id ", new { @id = user.Id }) == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SaveUser(User user)
        {
            try
            {
                _connection.Execute(@"Insert into Users Values (
                                @last_name,
                                @first_name,
                                @father_name,
                                @address,
                                @number_phone,
                                @role,
                                @salary,
                                @date_firts,
                                @login,
                                @password,
							     0)", new
                    {
                        @last_name = user.LastName,
                        @first_name = user.FirstName,
                        @father_name = user.FatherName,
                        @address = user.Address,
                        @number_phone = user.NumberPhone,
                        @role = user.Role,
                        @salary = user.Salary,
                        @date_firts = user.DateFirst,
                        @login = user.Login,
                        @password = user.Password
                    });
            }
            catch (Exception)
            {

            }
        }

        public bool IsNotDuplicate(string login)
        {
            try
            {
                var user = _connection.Query<User>("Select * from dbo.Users Where login = @login"
                                                   , new { @login = login }).FirstOrDefault();
                if (user != null)
                    return false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void UpdateUser(User user)
        {
            try
            {
                _connection.Execute(@"Update Users Set 
                                LastName = @last_name ,
                                FirstName = @first_name,
                                FatherName = @father_name,
                                Address = @address,
                                NumberPhone = @number_phone,
                                Role = @role,
                                Salary =@salary,
                                DateFirst = @date_firts,
                                Login = @login,
                                Password = @password,
							    IsDelete = @isDelete Where id = @id", new
                    {
                        @last_name = user.LastName,
                        @first_name = user.FirstName,
                        @father_name = user.FatherName,
                        @address = user.Address,
                        @number_phone = user.NumberPhone,
                        @role = user.Role,
                        @salary = user.Salary,
                        @date_firts = user.DateFirst,
                        @login = user.Login,
                        @password = user.Password,
                        @isDelet = user.IsDelete,
                        @id = user.Id
                    });
            }
            catch (Exception)
            {

            }

        }
    }
}
