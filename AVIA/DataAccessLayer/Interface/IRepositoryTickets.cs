﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccessLayer.Interface
{
    public interface IRepositoryTickets
    {

        IEnumerable<Ticket> GetAllPaidTickets();
        IEnumerable<Ticket> GetAllRefundTieckets();
        IEnumerable<Ticket> GetAllPaidTicketsByUser(User user);
        IEnumerable<Ticket> GetAllRefundTicketsByUser(User user);

        IEnumerable<Ticket> GetNotDeletedPaidTicketsByUser(User user);
        IEnumerable<Ticket> GetNotDeletedRefundTicketsByUser(User user);

        IEnumerable<Ticket> GetNotDeletedPaidTickets();
        IEnumerable<Ticket> GetNotDeletedRefundTickets();
        Ticket GetByIdPaidTicket(int id);
        Ticket GetByIdRefundTicket(int id);
        Ticket Save(Ticket ticket);
        bool DeletePaidTicket(Ticket ticket);
        bool DeleteRefundedTicket(Ticket ticket);
    }
}
