﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccessLayer.Interface
{
    public interface IRepositoryCreditTickets
    {
        IEnumerable<CreditTicket> GetAll();
        IEnumerable<CreditTicket> GetByIdTicket(int idTicket);

        CreditTicket GetCreditTicketById(int id);

        void Save(CreditTicket creditTicket);
        void Delete(CreditTicket creditTicket);
    }
}
