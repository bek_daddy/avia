﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccessLayer.Interface
{
  public  interface IRepositoryCompanies
  {
      IEnumerable<Company> GetAllCompanies();
      IEnumerable<Company> GetNotDeletedCompanies();

      Company GetCompanyById(int id);

      Company Save(Company company);

      void DeleteCompanies(Company company);
  }
}
