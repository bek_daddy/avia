﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccessLayer.Interface
{
    public interface IRepositoryUsers
    {
        IEnumerable<User> GetAllUsers();
        IEnumerable<User> GetNotDeleteUsers();
        User GetUserByLoginPassword(string login, string password);

        User GetUserById(int id);

        bool DeleteUser(User user);

        void SaveUser(User user);

        bool IsNotDuplicate(string login);

        void UpdateUser(User user);
    }
}
