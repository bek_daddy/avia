﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AVIA.Forms;
using AVIA.View;
using BusinessLogic.Implementation;
using BusinessLogic.Interface;

namespace AVIA.Presenter
{
   public class PresenterPaidTickets
   {
       private readonly IViewPaidTickets _viewPaidTickets;
       private readonly IServiceTickets _serviceTickets;

       public PresenterPaidTickets(IViewPaidTickets view)
       {
           _serviceTickets = new ServiceTickets();
           _viewPaidTickets = view;
           Setup();
       }

       private void Setup()
       {
           _viewPaidTickets.ChangedTicket += (sender, e) =>
               {
                   _viewPaidTickets.NewPaidTicket = _serviceTickets.CalculateNewTicket(_viewPaidTickets.NewPaidTicket);
               };

           _viewPaidTickets.DeletePaidTicket += (sender, e) =>
               {
                   
               };
       }

   }
}
