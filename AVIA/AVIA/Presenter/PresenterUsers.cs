﻿using AVIA.View;
using BusinessLogic.Implementation;
using BusinessLogic.Interface;

namespace AVIA.Presenter
{
    public class PresenterUsers
    {
        private readonly IServiceUsers _serviceUsers;
        private readonly IViewUsers _viewUsers;
        public PresenterUsers(IViewUsers viewUsers)
        {
            _serviceUsers = new ServiceUsers();
            _viewUsers = viewUsers;
            Setup();
        }

        private void Setup()
        {
            RefreshUsersList();
            _viewUsers.DeleteUser += (b, c) =>
                {
                    if (_serviceUsers.IsValidateForDelete(_viewUsers.RemoveUser))
                    {
                        if (_viewUsers.ShowMessage("Удаление...",
                                                   "Удалить -" + _viewUsers.RemoveUser.LastName + " " +
                                                   _viewUsers.RemoveUser.FirstName + " ?", "Result"))
                        {
                            if (_serviceUsers.DeleteUser(_viewUsers.RemoveUser))
                                _viewUsers.ShowMessage("Внимание..", "Пользователь удален!", "Show");
                            RefreshUsersList();
                        }
                    }
                    else
                        _viewUsers.ShowMessage("Ошибка..", "Указанный пользователь нельза удалить!", "Show");
                };

            _viewUsers.SaveUser += (b, c) =>
                {
                    if (_serviceUsers.IsValidateForSave(_viewUsers.NewUser))
                    {
                        if (_viewUsers.ShowMessage("Внимание..",
                                                   "Сохранить пользователя - " + _viewUsers.NewUser.GetFullName,
                                                   "Result"))
                            if (_serviceUsers.SaveNewUser(_viewUsers.NewUser) != null)
                            {
                                RefreshUsersList();
                                _viewUsers.ShowMessage("Сохранение...",
                                                       _viewUsers.NewUser.LastName + " " + _viewUsers.NewUser.FirstName +
                                                       " - успешно сохранен!", "Show");
                                _viewUsers.ResetData();
                            }
                    }
                    else
                        _viewUsers.ShowMessage("Ошибка..", "Заполните все поля!", "Show");
                };


            _viewUsers.IsValidate += (c, d) =>
                {
                    if (!_serviceUsers.IsDuplicate(_viewUsers.NewLogin))
                        _viewUsers.ShowMessage("Ошибка...", _viewUsers.NewLogin + " -такой логин уже занят!", "ShowAction");
                };
        }

        private void RefreshUsersList()
        {
            _viewUsers.SetUsers(_serviceUsers.GetAllUsers());
        }
    }
}
