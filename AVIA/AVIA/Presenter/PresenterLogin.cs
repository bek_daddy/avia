﻿using AVIA.Forms;
using AVIA.View;
using BusinessLogic.Implementation;
using BusinessLogic.Interface;
using Domain.Entities;

namespace AVIA.Presenter
{
    public class PresenterLogin
    {
        private readonly IViewLogin _viewLogin;
        private readonly IServiceUsers _serviceUsers;

        public PresenterLogin(IViewLogin viewLogin)
        {
            _viewLogin = viewLogin;
            _serviceUsers = new ServiceUsers();
            Setup();
        }

        private void Setup()
        {
            _viewLogin.LogIn += (c, b) =>
            {
                _serviceUsers.Login(_viewLogin.Login, _viewLogin.Password);
                if (ApplicationUser.CurrentUser != null)
                {
                    var view = new MainForm();
                    _viewLogin.HideForm();
                    view.ShowDialog();
                }
            };


        }



    }
}
