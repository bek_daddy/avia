﻿using System;

namespace AVIA.View
{
    public interface IViewLogin
    {
        string Login { get; set; }
        string Password { get; set; }

        event EventHandler<EventArgs> LogIn;

        void HideForm();
    }
}
