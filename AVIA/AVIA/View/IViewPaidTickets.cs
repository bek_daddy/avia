﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace AVIA.View
{
    public interface IViewPaidTickets
    {
        void SetAllPaidTickets(IEnumerable<Ticket> paidTickets);
        void SetAllCompanies(IEnumerable<Company> companies);

        PaidTicket RemovePaidTicket { get; }
        PaidTicket NewPaidTicket { get; set; }

        event EventHandler<EventArgs> DeletePaidTicket;
        event EventHandler<EventArgs> SavePaidTicket;
        event EventHandler<EventArgs> ChangedTicket;
    }
}
