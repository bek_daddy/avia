﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace AVIA.View
{
  public  interface IViewUsers
  {
      void SetUsers(IEnumerable<User> users);

      User NewUser { get; }
      User RemoveUser { get; }


      event EventHandler<EventArgs> SaveUser;

      event EventHandler<EventArgs> DeleteUser;

      event EventHandler<EventArgs> IsValidate;

      bool ShowMessage(string caption, string body, string code);

      string NewLogin { get;  }

      void ResetData();

  }
}
