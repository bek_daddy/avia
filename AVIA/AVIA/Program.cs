﻿using System;
using System.Windows.Forms;
using AVIA.Forms;
using AVIA.Presenter;

namespace AVIA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new LoginForm();
            var presenter = new PresenterLogin(form);
            Application.Run(form);
        }
    }


}