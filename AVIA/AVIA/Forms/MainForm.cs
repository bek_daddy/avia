﻿using System.Windows.Forms;
using AVIA.Presenter;
using Telerik.WinControls.UI;

namespace AVIA.Forms
{
    public partial class MainForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public MainForm()
        {
            InitializeComponent();

            Setup();
        }

        private void Setup()
        {
            Closing += (c, d)
                             =>
                {
                    if (MessageBox.Show("Выйти из программы?", "Выход...", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        Application.Exit();
                    d.Cancel = true;
                };

            _users.Click += (c, b) =>
                {
                    var view = new UsersForm();
                    var presenter = new PresenterUsers(view);
                    view.ShowDialog();
                };

            _paidTickets.Click += (sender, e) =>
                {
                    var view = new PaidTicketsForm();
                    var presenter = new PresenterPaidTickets(view);
                    view.ShowDialog();
                };

            LoadPhoto();
        }



        private void LoadPhoto()
        {
            _users.ContentChangeInterval = 7000;
            _users.TransitionType = ContentTransitionType.SlideUp;
            _users.BackgroundImage = Properties.Resources.post_unhappy_workers;

        }
    }
}