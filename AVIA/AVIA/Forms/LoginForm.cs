﻿using System;
using System.Threading;
using System.Windows.Forms;
using AVIA.View;

namespace AVIA.Forms
{
    public partial class LoginForm : Form, IViewLogin
    {
        public LoginForm()
        {
            InitializeComponent();

            _buttonClose.Click += (b, c) => Close();

            _buttonLogin.ValueChanged += (b, c) =>
                {
                    if (_buttonLogin.Value && !string.IsNullOrEmpty(_textBoxLogin.Text.Trim()) && !string.IsNullOrEmpty(_textBoxPassword.Text.Trim()))
                    {
                        LogIn(this, EventArgs.Empty);
                    }
                    Thread.Sleep(200);
                    _buttonLogin.Value = false;
                };
        }
        #region  View
        public string Login
        {
            get { return _textBoxLogin.Text.Trim(); }
            set { }
        }
        public string Password
        {
            get { return _textBoxPassword.Text.Trim(); }
            set { }
        }
        public event EventHandler<EventArgs> LogIn;

        public void HideForm()
        {
            this.Hide();
        }

        #endregion

    }
}
