﻿namespace AVIA.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._panorama = new Telerik.WinControls.UI.RadPanorama();
            this._users = new Telerik.WinControls.UI.RadLiveTileElement();
            this._paidTickets = new Telerik.WinControls.UI.RadTileElement();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._panorama)).BeginInit();
            this.SuspendLayout();
            // 
            // _panorama
            // 
            this._panorama.BackColor = System.Drawing.Color.White;
            this._panorama.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_panorama.BackgroundImage")));
            this._panorama.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._panorama.Cursor = System.Windows.Forms.Cursors.Hand;
            this._panorama.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panorama.ForeColor = System.Drawing.Color.Black;
            this._panorama.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._users,
            this._paidTickets});
            this._panorama.Location = new System.Drawing.Point(0, 0);
            this._panorama.Name = "_panorama";
            this._panorama.RowsCount = 8;
            this._panorama.ScrollBarAlignment = Telerik.WinControls.UI.HorizontalScrollAlignment.Bottom;
            this._panorama.Size = new System.Drawing.Size(754, 424);
            this._panorama.TabIndex = 0;
            this._panorama.Text = "radPanorama1";
            this._panorama.ThemeName = "ControlDefault";
            // 
            // _users
            // 
            this._users.AccessibleDescription = "Пользователи";
            this._users.AccessibleName = "Пользователи";
            this._users.ColSpan = 4;
            this._users.Column = 1;
            this._users.Font = new System.Drawing.Font("Segoe UI Light", 20F);
            this._users.Name = "_users";
            this._users.Row = 1;
            this._users.Text = "Пользователи";
            this._users.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this._users.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // _paidTickets
            // 
            this._paidTickets.AccessibleDescription = "Авиа билеты";
            this._paidTickets.AccessibleName = "Авиа билеты";
            this._paidTickets.ColSpan = 3;
            this._paidTickets.Column = 1;
            this._paidTickets.Font = new System.Drawing.Font("Segoe UI Light", 24F);
            this._paidTickets.Name = "_paidTickets";
            this._paidTickets.Row = 2;
            this._paidTickets.Text = "Авиа билеты";
            this._paidTickets.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this._paidTickets.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(754, 424);
            this.Controls.Add(this._panorama);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            ((System.ComponentModel.ISupportInitialize)(this._panorama)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanorama _panorama;
        private Telerik.WinControls.UI.RadLiveTileElement _users;
        private Telerik.WinControls.UI.RadTileElement _paidTickets;
        private DevComponents.DotNetBar.StyleManager styleManager1;
    }
}