﻿namespace AVIA.Forms
{
    partial class TicketDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TicketDetailsForm));
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.ribbonClientPanel1 = new DevComponents.DotNetBar.Ribbon.RibbonClientPanel();
            this._buttonSave = new DevComponents.DotNetBar.ButtonX();
            this._comboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this._inputDopCommissia = new DevComponents.Editors.DoubleInput();
            this._inputItogo = new DevComponents.Editors.DoubleInput();
            this._inputOplataPassajir = new DevComponents.Editors.DoubleInput();
            this._inputSkidka = new DevComponents.Editors.DoubleInput();
            this._inputCommissiaSom = new DevComponents.Editors.DoubleInput();
            this._inputCommissia = new DevComponents.Editors.DoubleInput();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._inputItogoPoBiletu = new DevComponents.Editors.DoubleInput();
            this.label13 = new System.Windows.Forms.Label();
            this._inputAeroportSbor = new DevComponents.Editors.DoubleInput();
            this.label12 = new System.Windows.Forms.Label();
            this._inputTopSbor = new DevComponents.Editors.DoubleInput();
            this.label11 = new System.Windows.Forms.Label();
            this._inputRate = new DevComponents.Editors.DoubleInput();
            this.label10 = new System.Windows.Forms.Label();
            this._inputValuTarif = new DevComponents.Editors.DoubleInput();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxReis = new Telerik.WinControls.UI.RadTextBoxControl();
            this.label1 = new System.Windows.Forms.Label();
            this._textBoxDirection = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxFIO = new Telerik.WinControls.UI.RadTextBoxControl();
            this.label16 = new System.Windows.Forms.Label();
            this._inputTarifSom = new DevComponents.Editors.DoubleInput();
            this._buttonCredit = new DevComponents.DotNetBar.ButtonX();
            this.ribbonClientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputDopCommissia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputItogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputOplataPassajir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputSkidka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputCommissiaSom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputCommissia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputItogoPoBiletu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputAeroportSbor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputTopSbor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputValuTarif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxReis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputTarifSom)).BeginInit();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // ribbonClientPanel1
            // 
            this.ribbonClientPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.ribbonClientPanel1.Controls.Add(this._buttonCredit);
            this.ribbonClientPanel1.Controls.Add(this._buttonSave);
            this.ribbonClientPanel1.Controls.Add(this._comboCompany);
            this.ribbonClientPanel1.Controls.Add(this._inputDopCommissia);
            this.ribbonClientPanel1.Controls.Add(this._inputItogo);
            this.ribbonClientPanel1.Controls.Add(this._inputOplataPassajir);
            this.ribbonClientPanel1.Controls.Add(this._inputSkidka);
            this.ribbonClientPanel1.Controls.Add(this._inputCommissiaSom);
            this.ribbonClientPanel1.Controls.Add(this._inputCommissia);
            this.ribbonClientPanel1.Controls.Add(this.label14);
            this.ribbonClientPanel1.Controls.Add(this.label15);
            this.ribbonClientPanel1.Controls.Add(this._inputItogoPoBiletu);
            this.ribbonClientPanel1.Controls.Add(this.label13);
            this.ribbonClientPanel1.Controls.Add(this._inputAeroportSbor);
            this.ribbonClientPanel1.Controls.Add(this.label12);
            this.ribbonClientPanel1.Controls.Add(this._inputTopSbor);
            this.ribbonClientPanel1.Controls.Add(this.label11);
            this.ribbonClientPanel1.Controls.Add(this._inputTarifSom);
            this.ribbonClientPanel1.Controls.Add(this._inputRate);
            this.ribbonClientPanel1.Controls.Add(this.label10);
            this.ribbonClientPanel1.Controls.Add(this._inputValuTarif);
            this.ribbonClientPanel1.Controls.Add(this.label9);
            this.ribbonClientPanel1.Controls.Add(this.label6);
            this.ribbonClientPanel1.Controls.Add(this.label3);
            this.ribbonClientPanel1.Controls.Add(this.label8);
            this.ribbonClientPanel1.Controls.Add(this.label4);
            this.ribbonClientPanel1.Controls.Add(this.label16);
            this.ribbonClientPanel1.Controls.Add(this.label7);
            this.ribbonClientPanel1.Controls.Add(this.label5);
            this.ribbonClientPanel1.Controls.Add(this.label2);
            this.ribbonClientPanel1.Controls.Add(this._textBoxReis);
            this.ribbonClientPanel1.Controls.Add(this.label1);
            this.ribbonClientPanel1.Controls.Add(this._textBoxDirection);
            this.ribbonClientPanel1.Controls.Add(this._textBoxFIO);
            this.ribbonClientPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonClientPanel1.Location = new System.Drawing.Point(0, 0);
            this.ribbonClientPanel1.Name = "ribbonClientPanel1";
            this.ribbonClientPanel1.Size = new System.Drawing.Size(454, 561);
            // 
            // 
            // 
            this.ribbonClientPanel1.Style.Class = "RibbonClientPanel";
            this.ribbonClientPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonClientPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonClientPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonClientPanel1.TabIndex = 0;
            // 
            // _buttonSave
            // 
            this._buttonSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this._buttonSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._buttonSave.BackColor = System.Drawing.Color.Transparent;
            this._buttonSave.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("_buttonSave.Image")));
            this._buttonSave.Location = new System.Drawing.Point(46, 513);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(22, 22, 2, 2);
            this._buttonSave.Size = new System.Drawing.Size(213, 45);
            this._buttonSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._buttonSave.TabIndex = 16;
            this._buttonSave.Text = "СОХРАНИТЬ";
            this._buttonSave.TextColor = System.Drawing.Color.DarkOrange;
            // 
            // _comboCompany
            // 
            this._comboCompany.DisplayMember = "Name";
            this._comboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._comboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboCompany.ForeColor = System.Drawing.Color.Black;
            this._comboCompany.FormattingEnabled = true;
            this._comboCompany.ItemHeight = 16;
            this._comboCompany.Location = new System.Drawing.Point(187, 419);
            this._comboCompany.Name = "_comboCompany";
            this._comboCompany.Size = new System.Drawing.Size(256, 22);
            this._comboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._comboCompany.TabIndex = 14;
            this._comboCompany.ValueMember = "ID";
            // 
            // _inputDopCommissia
            // 
            this._inputDopCommissia.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputDopCommissia.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputDopCommissia.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputDopCommissia.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputDopCommissia.ForeColor = System.Drawing.Color.Black;
            this._inputDopCommissia.Increment = 1D;
            this._inputDopCommissia.Location = new System.Drawing.Point(187, 447);
            this._inputDopCommissia.MinValue = 0D;
            this._inputDopCommissia.Name = "_inputDopCommissia";
            this._inputDopCommissia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._inputDopCommissia.ShowUpDown = true;
            this._inputDopCommissia.Size = new System.Drawing.Size(256, 22);
            this._inputDopCommissia.TabIndex = 15;
            // 
            // _inputItogo
            // 
            this._inputItogo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputItogo.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputItogo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputItogo.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputItogo.ForeColor = System.Drawing.Color.Black;
            this._inputItogo.Increment = 1D;
            this._inputItogo.Location = new System.Drawing.Point(187, 391);
            this._inputItogo.MinValue = 0D;
            this._inputItogo.Name = "_inputItogo";
            this._inputItogo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._inputItogo.ShowUpDown = true;
            this._inputItogo.Size = new System.Drawing.Size(256, 22);
            this._inputItogo.TabIndex = 13;
            // 
            // _inputOplataPassajir
            // 
            this._inputOplataPassajir.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputOplataPassajir.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputOplataPassajir.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputOplataPassajir.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputOplataPassajir.ForeColor = System.Drawing.Color.Black;
            this._inputOplataPassajir.Increment = 1D;
            this._inputOplataPassajir.Location = new System.Drawing.Point(187, 363);
            this._inputOplataPassajir.MinValue = 0D;
            this._inputOplataPassajir.Name = "_inputOplataPassajir";
            this._inputOplataPassajir.ShowUpDown = true;
            this._inputOplataPassajir.Size = new System.Drawing.Size(256, 22);
            this._inputOplataPassajir.TabIndex = 12;
            // 
            // _inputSkidka
            // 
            this._inputSkidka.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputSkidka.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputSkidka.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputSkidka.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputSkidka.ForeColor = System.Drawing.Color.Black;
            this._inputSkidka.Increment = 1D;
            this._inputSkidka.Location = new System.Drawing.Point(187, 335);
            this._inputSkidka.MinValue = 0D;
            this._inputSkidka.Name = "_inputSkidka";
            this._inputSkidka.ShowUpDown = true;
            this._inputSkidka.Size = new System.Drawing.Size(256, 22);
            this._inputSkidka.TabIndex = 11;
            // 
            // _inputCommissiaSom
            // 
            this._inputCommissiaSom.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputCommissiaSom.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputCommissiaSom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputCommissiaSom.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputCommissiaSom.ForeColor = System.Drawing.Color.Black;
            this._inputCommissiaSom.Increment = 1D;
            this._inputCommissiaSom.Location = new System.Drawing.Point(187, 307);
            this._inputCommissiaSom.MinValue = 0D;
            this._inputCommissiaSom.Name = "_inputCommissiaSom";
            this._inputCommissiaSom.ShowUpDown = true;
            this._inputCommissiaSom.Size = new System.Drawing.Size(256, 22);
            this._inputCommissiaSom.TabIndex = 10;
            // 
            // _inputCommissia
            // 
            this._inputCommissia.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputCommissia.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputCommissia.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputCommissia.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputCommissia.ForeColor = System.Drawing.Color.Black;
            this._inputCommissia.Increment = 1D;
            this._inputCommissia.Location = new System.Drawing.Point(187, 279);
            this._inputCommissia.MinValue = 0D;
            this._inputCommissia.Name = "_inputCommissia";
            this._inputCommissia.ShowUpDown = true;
            this._inputCommissia.Size = new System.Drawing.Size(256, 22);
            this._inputCommissia.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(12, 422);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 19);
            this.label14.TabIndex = 1;
            this.label14.Text = "Авиа компания";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(12, 450);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 19);
            this.label15.TabIndex = 1;
            this.label15.Text = "Доп. коммиссия";
            // 
            // _inputItogoPoBiletu
            // 
            this._inputItogoPoBiletu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputItogoPoBiletu.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputItogoPoBiletu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputItogoPoBiletu.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputItogoPoBiletu.ForeColor = System.Drawing.Color.Black;
            this._inputItogoPoBiletu.Increment = 1D;
            this._inputItogoPoBiletu.Location = new System.Drawing.Point(187, 251);
            this._inputItogoPoBiletu.MinValue = 0D;
            this._inputItogoPoBiletu.Name = "_inputItogoPoBiletu";
            this._inputItogoPoBiletu.ShowUpDown = true;
            this._inputItogoPoBiletu.Size = new System.Drawing.Size(256, 22);
            this._inputItogoPoBiletu.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(12, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 19);
            this.label13.TabIndex = 1;
            this.label13.Text = "Итого";
            // 
            // _inputAeroportSbor
            // 
            this._inputAeroportSbor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputAeroportSbor.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputAeroportSbor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputAeroportSbor.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputAeroportSbor.ForeColor = System.Drawing.Color.Black;
            this._inputAeroportSbor.Increment = 1D;
            this._inputAeroportSbor.Location = new System.Drawing.Point(187, 223);
            this._inputAeroportSbor.MinValue = 0D;
            this._inputAeroportSbor.Name = "_inputAeroportSbor";
            this._inputAeroportSbor.ShowUpDown = true;
            this._inputAeroportSbor.Size = new System.Drawing.Size(256, 22);
            this._inputAeroportSbor.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(12, 366);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 19);
            this.label12.TabIndex = 1;
            this.label12.Text = "Оплата пассажира";
            // 
            // _inputTopSbor
            // 
            this._inputTopSbor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputTopSbor.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputTopSbor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputTopSbor.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputTopSbor.ForeColor = System.Drawing.Color.Black;
            this._inputTopSbor.Increment = 1D;
            this._inputTopSbor.Location = new System.Drawing.Point(187, 195);
            this._inputTopSbor.MinValue = 0D;
            this._inputTopSbor.Name = "_inputTopSbor";
            this._inputTopSbor.ShowUpDown = true;
            this._inputTopSbor.Size = new System.Drawing.Size(256, 22);
            this._inputTopSbor.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(12, 338);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 19);
            this.label11.TabIndex = 1;
            this.label11.Text = "Скидка";
            // 
            // _inputRate
            // 
            this._inputRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputRate.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputRate.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputRate.ForeColor = System.Drawing.Color.Black;
            this._inputRate.Increment = 1D;
            this._inputRate.Location = new System.Drawing.Point(187, 138);
            this._inputRate.MinValue = 0D;
            this._inputRate.Name = "_inputRate";
            this._inputRate.ShowUpDown = true;
            this._inputRate.Size = new System.Drawing.Size(256, 22);
            this._inputRate.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(12, 310);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 19);
            this.label10.TabIndex = 1;
            this.label10.Text = "Коммиссия(сом)";
            // 
            // _inputValuTarif
            // 
            this._inputValuTarif.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputValuTarif.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputValuTarif.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputValuTarif.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputValuTarif.ForeColor = System.Drawing.Color.Black;
            this._inputValuTarif.Increment = 1D;
            this._inputValuTarif.Location = new System.Drawing.Point(187, 110);
            this._inputValuTarif.MinValue = 0D;
            this._inputValuTarif.Name = "_inputValuTarif";
            this._inputValuTarif.ShowUpDown = true;
            this._inputValuTarif.Size = new System.Drawing.Size(256, 22);
            this._inputValuTarif.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(12, 282);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 19);
            this.label9.TabIndex = 1;
            this.label9.Text = "Коммиссия(%)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(12, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "Сбор(топ)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Рейс";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(12, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 19);
            this.label8.TabIndex = 1;
            this.label8.Text = "Сбор(аэр)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Тариф(вал)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(12, 254);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Итого по билету";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 19);
            this.label5.TabIndex = 1;
            this.label5.Text = "Курс валюты";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Направление";
            // 
            // _textBoxReis
            // 
            this._textBoxReis.BackColor = System.Drawing.Color.White;
            this._textBoxReis.ForeColor = System.Drawing.Color.Black;
            this._textBoxReis.Location = new System.Drawing.Point(187, 66);
            this._textBoxReis.Name = "_textBoxReis";
            this._textBoxReis.Size = new System.Drawing.Size(256, 26);
            this._textBoxReis.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Harrington", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ф.И.О.";
            // 
            // _textBoxDirection
            // 
            this._textBoxDirection.BackColor = System.Drawing.Color.White;
            this._textBoxDirection.ForeColor = System.Drawing.Color.Black;
            this._textBoxDirection.Location = new System.Drawing.Point(187, 36);
            this._textBoxDirection.Name = "_textBoxDirection";
            this._textBoxDirection.Size = new System.Drawing.Size(256, 26);
            this._textBoxDirection.TabIndex = 1;
            // 
            // _textBoxFIO
            // 
            this._textBoxFIO.BackColor = System.Drawing.Color.White;
            this._textBoxFIO.ForeColor = System.Drawing.Color.Black;
            this._textBoxFIO.Location = new System.Drawing.Point(122, 3);
            this._textBoxFIO.Name = "_textBoxFIO";
            this._textBoxFIO.Size = new System.Drawing.Size(321, 26);
            this._textBoxFIO.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(12, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 19);
            this.label16.TabIndex = 1;
            this.label16.Text = "Тариф(сом)";
            // 
            // _inputTarifSom
            // 
            this._inputTarifSom.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputTarifSom.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputTarifSom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputTarifSom.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputTarifSom.ForeColor = System.Drawing.Color.Black;
            this._inputTarifSom.Increment = 1D;
            this._inputTarifSom.Location = new System.Drawing.Point(187, 166);
            this._inputTarifSom.MinValue = 0D;
            this._inputTarifSom.Name = "_inputTarifSom";
            this._inputTarifSom.ShowUpDown = true;
            this._inputTarifSom.Size = new System.Drawing.Size(256, 22);
            this._inputTarifSom.TabIndex = 5;
            // 
            // _buttonCredit
            // 
            this._buttonCredit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this._buttonCredit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._buttonCredit.BackColor = System.Drawing.Color.Transparent;
            this._buttonCredit.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this._buttonCredit.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonCredit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._buttonCredit.Image = ((System.Drawing.Image)(resources.GetObject("_buttonCredit.Image")));
            this._buttonCredit.Location = new System.Drawing.Point(279, 513);
            this._buttonCredit.Name = "_buttonCredit";
            this._buttonCredit.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(22, 22, 2, 2);
            this._buttonCredit.Size = new System.Drawing.Size(163, 45);
            this._buttonCredit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._buttonCredit.TabIndex = 17;
            this._buttonCredit.Text = "В долг";
            this._buttonCredit.TextColor = System.Drawing.Color.DarkOrange;
            // 
            // TicketDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 561);
            this.Controls.Add(this.ribbonClientPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximumSize = new System.Drawing.Size(470, 600);
            this.MinimumSize = new System.Drawing.Size(470, 600);
            this.Name = "TicketDetailsForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Детали";
            this.ribbonClientPanel1.ResumeLayout(false);
            this.ribbonClientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputDopCommissia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputItogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputOplataPassajir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputSkidka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputCommissiaSom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputCommissia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputItogoPoBiletu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputAeroportSbor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputTopSbor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputValuTarif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxReis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputTarifSom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.Ribbon.RibbonClientPanel ribbonClientPanel1;
        private DevComponents.Editors.DoubleInput _inputCommissia;
        private DevComponents.Editors.DoubleInput _inputItogoPoBiletu;
        private DevComponents.Editors.DoubleInput _inputAeroportSbor;
        private DevComponents.Editors.DoubleInput _inputTopSbor;
        private DevComponents.Editors.DoubleInput _inputRate;
        private DevComponents.Editors.DoubleInput _inputValuTarif;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxReis;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxDirection;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxFIO;
        private DevComponents.DotNetBar.Controls.ComboBoxEx _comboCompany;
        private DevComponents.Editors.DoubleInput _inputItogo;
        private DevComponents.Editors.DoubleInput _inputOplataPassajir;
        private DevComponents.Editors.DoubleInput _inputSkidka;
        private DevComponents.Editors.DoubleInput _inputCommissiaSom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private DevComponents.DotNetBar.ButtonX _buttonSave;
        private DevComponents.Editors.DoubleInput _inputDopCommissia;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.ButtonX _buttonCredit;
        private DevComponents.Editors.DoubleInput _inputTarifSom;
        private System.Windows.Forms.Label label16;

    }
}