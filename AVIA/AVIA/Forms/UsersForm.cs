﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AVIA.View;
using Domain.Entities;

namespace AVIA.Forms
{
    public partial class UsersForm : DevComponents.DotNetBar.Metro.MetroForm, IViewUsers
    {
        public UsersForm()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            _dateFirstColumn.AspectToStringConverter = (value) => ((DateTime)value).ToString("dd/MM/yyyy");

            _textBoxLogin.LostFocus += (c, b) =>
                {
                    if (!string.IsNullOrEmpty(_textBoxLogin.Text.Trim()))
                        IsValidate(this, EventArgs.Empty);
                };

            _buttonReset.Click += (c, b) => ResetData();

            _buttonSave.Click += (c, b) => SaveUser(this, EventArgs.Empty);

            _menuUsers.Items[0].Click += (sender, e) => DeleteUser(this, EventArgs.Empty);
        }


        #region View
        public void SetUsers(IEnumerable<User> users)
        {
            _usersList.SetObjects(users);
        }

        public User NewUser
        {
            get
            {
                return new User()
                    {
                        LastName = _textBoxLastName.Text.Trim(),
                        FirstName = _textBoxFirstName.Text.Trim(),
                        FatherName = _textBoxFatherName.Text.Trim(),
                        Address = _textBoxAddress.Text.Trim(),
                        DateFirst = _dateFirst.Value,
                        Login = _textBoxLogin.Text.Trim(),
                        NumberPhone = _textBoxNumberPhone.Text.Trim(),
                        Password = _textBoxPassword.Text.Trim(),
                        Role = _dBoxRole.Text,
                        Salary = _inputSalary.Value
                    };
            }
        }

        User IViewUsers.RemoveUser
        {
            get
            {
                return (User)_usersList.SelectedItem.RowObject;
            }
        }

        public event EventHandler<EventArgs> SaveUser;
        public event EventHandler<EventArgs> DeleteUser;
        public event EventHandler<EventArgs> IsValidate;


        public bool ShowMessage(string caption, string body, string code)
        {
            if (code == "Result" && MessageBox.Show(body, caption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                return true;

            if (code == "Show")
            {
                MessageBox.Show(body, caption);
            }

            if (code == "ShowAction")
            {
                MessageBox.Show(body, caption);
                _textBoxLogin.Text = string.Empty;
                _textBoxLogin.Focus();

            }
            return false;
        }

        public string NewLogin { get { return _textBoxLogin.Text.Trim(); } }

        public void ResetData()
        {
            _textBoxLastName.Text =
                 _textBoxFirstName.Text =
                 _textBoxFatherName.Text =
                 _textBoxAddress.Text =
                 _textBoxNumberPhone.Text =
                 _textBoxLogin.Text =
                 _textBoxPassword.Text = string.Empty;
            _inputSalary.Value = 100;
            _dBoxRole.SelectedIndex = -1;
            _dateFirst.Value = DateTime.Now;
        }

        #endregion
    }
}