﻿namespace AVIA.Forms
{
    partial class PaidTicketsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaidTicketsForm));
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this._ticketsList = new BrightIdeasSoftware.ObjectListView();
            this._colFio = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colDirection = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colReis = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colValutTarif = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colRate = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colTopSbor = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colAeroportSbor = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colItogoPoBiletu = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colCommission = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colCommissionSom = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colSkidka = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colOplataPassajir = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colItogo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colCompany = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._colCredit = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._lastNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._firstNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._fatherNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._addressColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._numberPhoneColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._roleColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._dateFirstColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._loginColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._passwordColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._isDeleteColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this._buttonDelete = new DevComponents.DotNetBar.ButtonX();
            this._buttonAdd = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this._ticketsList)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Windows7Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // _ticketsList
            // 
            this._ticketsList.AllColumns.Add(this._colFio);
            this._ticketsList.AllColumns.Add(this._colDirection);
            this._ticketsList.AllColumns.Add(this._colReis);
            this._ticketsList.AllColumns.Add(this._colValutTarif);
            this._ticketsList.AllColumns.Add(this._colRate);
            this._ticketsList.AllColumns.Add(this._colTopSbor);
            this._ticketsList.AllColumns.Add(this._colAeroportSbor);
            this._ticketsList.AllColumns.Add(this._colItogoPoBiletu);
            this._ticketsList.AllColumns.Add(this._colCommission);
            this._ticketsList.AllColumns.Add(this._colCommissionSom);
            this._ticketsList.AllColumns.Add(this._colSkidka);
            this._ticketsList.AllColumns.Add(this._colOplataPassajir);
            this._ticketsList.AllColumns.Add(this._colItogo);
            this._ticketsList.AllColumns.Add(this._colCompany);
            this._ticketsList.AllColumns.Add(this._colCredit);
            this._ticketsList.BackColor = System.Drawing.Color.White;
            this._ticketsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._ticketsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._colFio,
            this._colDirection,
            this._colReis,
            this._colValutTarif,
            this._colRate,
            this._colTopSbor,
            this._colAeroportSbor,
            this._colItogoPoBiletu,
            this._colCommission,
            this._colCommissionSom,
            this._colSkidka,
            this._colOplataPassajir,
            this._colItogo,
            this._colCompany,
            this._colCredit});
            this._ticketsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ticketsList.ForeColor = System.Drawing.Color.Black;
            this._ticketsList.FullRowSelect = true;
            this._ticketsList.GridLines = true;
            this._ticketsList.Location = new System.Drawing.Point(30, 0);
            this._ticketsList.Name = "_ticketsList";
            this._ticketsList.ShowGroups = false;
            this._ticketsList.Size = new System.Drawing.Size(1188, 443);
            this._ticketsList.TabIndex = 2;
            this._ticketsList.UseCompatibleStateImageBehavior = false;
            this._ticketsList.View = System.Windows.Forms.View.Details;
            // 
            // _colFio
            // 
            this._colFio.AspectName = "FIO";
            this._colFio.CellPadding = null;
            this._colFio.Text = "Ф.И.О.";
            this._colFio.Width = 129;
            // 
            // _colDirection
            // 
            this._colDirection.AspectName = "Direction";
            this._colDirection.CellPadding = null;
            this._colDirection.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colDirection.Text = "Направление";
            this._colDirection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colDirection.Width = 84;
            // 
            // _colReis
            // 
            this._colReis.AspectName = "Reis";
            this._colReis.CellPadding = null;
            this._colReis.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colReis.Text = "Рейс";
            this._colReis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colReis.Width = 49;
            // 
            // _colValutTarif
            // 
            this._colValutTarif.AspectName = "ValutTarif";
            this._colValutTarif.CellPadding = null;
            this._colValutTarif.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colValutTarif.Text = "Тариф(вал)";
            this._colValutTarif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colValutTarif.Width = 76;
            // 
            // _colRate
            // 
            this._colRate.AspectName = "Rate";
            this._colRate.CellPadding = null;
            this._colRate.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colRate.Text = "Курс";
            this._colRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colRate.Width = 43;
            // 
            // _colTopSbor
            // 
            this._colTopSbor.AspectName = "TopSbor";
            this._colTopSbor.CellPadding = null;
            this._colTopSbor.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colTopSbor.Text = "Сбор (топ)";
            this._colTopSbor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colTopSbor.Width = 74;
            // 
            // _colAeroportSbor
            // 
            this._colAeroportSbor.AspectName = "AeroportSbor";
            this._colAeroportSbor.CellPadding = null;
            this._colAeroportSbor.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colAeroportSbor.Text = "Сбор (аэр)";
            this._colAeroportSbor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colAeroportSbor.Width = 83;
            // 
            // _colItogoPoBiletu
            // 
            this._colItogoPoBiletu.AspectName = "ItogoPoBiletu";
            this._colItogoPoBiletu.CellPadding = null;
            this._colItogoPoBiletu.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colItogoPoBiletu.Text = "Итого по билету";
            this._colItogoPoBiletu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colItogoPoBiletu.Width = 106;
            // 
            // _colCommission
            // 
            this._colCommission.AspectName = "Commission";
            this._colCommission.CellPadding = null;
            this._colCommission.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCommission.Text = "Коммиссия";
            this._colCommission.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCommission.Width = 73;
            // 
            // _colCommissionSom
            // 
            this._colCommissionSom.AspectName = "CommissionSom";
            this._colCommissionSom.CellPadding = null;
            this._colCommissionSom.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCommissionSom.Text = "Коммиссия(сом)";
            this._colCommissionSom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCommissionSom.Width = 103;
            // 
            // _colSkidka
            // 
            this._colSkidka.AspectName = "Skidka";
            this._colSkidka.CellPadding = null;
            this._colSkidka.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colSkidka.Text = "Скидка";
            this._colSkidka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _colOplataPassajir
            // 
            this._colOplataPassajir.AspectName = "OplataPassajir";
            this._colOplataPassajir.CellPadding = null;
            this._colOplataPassajir.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colOplataPassajir.Text = "Оплата пасс.";
            this._colOplataPassajir.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colOplataPassajir.Width = 85;
            // 
            // _colItogo
            // 
            this._colItogo.AspectName = "Itogo";
            this._colItogo.CellPadding = null;
            this._colItogo.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colItogo.Text = "Итого";
            this._colItogo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _colCompany
            // 
            this._colCompany.AspectName = "Company";
            this._colCompany.CellPadding = null;
            this._colCompany.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCompany.Text = "Компания";
            this._colCompany.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _colCredit
            // 
            this._colCredit.AspectName = "IsCredit";
            this._colCredit.CellPadding = null;
            this._colCredit.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._colCredit.Text = "В долг";
            this._colCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _lastNameColumn
            // 
            this._lastNameColumn.AspectName = "LastName";
            this._lastNameColumn.CellPadding = null;
            this._lastNameColumn.DisplayIndex = 0;
            this._lastNameColumn.Text = "Фамилия";
            this._lastNameColumn.Width = 142;
            // 
            // _firstNameColumn
            // 
            this._firstNameColumn.AspectName = "FirstName";
            this._firstNameColumn.CellPadding = null;
            this._firstNameColumn.DisplayIndex = 1;
            this._firstNameColumn.Text = "Имя";
            this._firstNameColumn.Width = 115;
            // 
            // _fatherNameColumn
            // 
            this._fatherNameColumn.AspectName = "FatherName";
            this._fatherNameColumn.CellPadding = null;
            this._fatherNameColumn.DisplayIndex = 2;
            this._fatherNameColumn.Text = "Отчество";
            this._fatherNameColumn.Width = 92;
            // 
            // _addressColumn
            // 
            this._addressColumn.AspectName = "Address";
            this._addressColumn.CellPadding = null;
            this._addressColumn.DisplayIndex = 3;
            this._addressColumn.Text = "Адрес";
            this._addressColumn.Width = 168;
            // 
            // _numberPhoneColumn
            // 
            this._numberPhoneColumn.AspectName = "NumberPhone";
            this._numberPhoneColumn.CellPadding = null;
            this._numberPhoneColumn.DisplayIndex = 4;
            this._numberPhoneColumn.Text = "Номер тел.";
            this._numberPhoneColumn.Width = 98;
            // 
            // _roleColumn
            // 
            this._roleColumn.AspectName = "Role";
            this._roleColumn.CellPadding = null;
            this._roleColumn.DisplayIndex = 5;
            this._roleColumn.Text = "Позиция";
            this._roleColumn.Width = 85;
            // 
            // _dateFirstColumn
            // 
            this._dateFirstColumn.AspectName = "DateFirst";
            this._dateFirstColumn.CellPadding = null;
            this._dateFirstColumn.DisplayIndex = 6;
            this._dateFirstColumn.Text = "Дата ";
            this._dateFirstColumn.Width = 76;
            // 
            // _loginColumn
            // 
            this._loginColumn.AspectName = "Login";
            this._loginColumn.CellPadding = null;
            this._loginColumn.DisplayIndex = 7;
            this._loginColumn.Text = "Логин";
            this._loginColumn.Width = 103;
            // 
            // _passwordColumn
            // 
            this._passwordColumn.AspectName = "Password";
            this._passwordColumn.CellPadding = null;
            this._passwordColumn.DisplayIndex = 8;
            this._passwordColumn.Text = "Пароль";
            this._passwordColumn.Width = 73;
            // 
            // _isDeleteColumn
            // 
            this._isDeleteColumn.AspectName = "IsDelete";
            this._isDeleteColumn.CellPadding = null;
            this._isDeleteColumn.DisplayIndex = 9;
            this._isDeleteColumn.Text = "Уделение";
            this._isDeleteColumn.Width = 62;
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.CollapseDirection = DevComponents.DotNetBar.eCollapseDirection.RightToLeft;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.expandablePanel1.Controls.Add(this._buttonDelete);
            this.expandablePanel1.Controls.Add(this._buttonAdd);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.expandablePanel1.Expanded = false;
            this.expandablePanel1.ExpandedBounds = new System.Drawing.Rectangle(0, 0, 200, 443);
            this.expandablePanel1.HideControlsWhenCollapsed = true;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(30, 443);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 3;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Меню";
            // 
            // _buttonDelete
            // 
            this._buttonDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this._buttonDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._buttonDelete.Image = ((System.Drawing.Image)(resources.GetObject("_buttonDelete.Image")));
            this._buttonDelete.Location = new System.Drawing.Point(12, 116);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(174, 43);
            this._buttonDelete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._buttonDelete.TabIndex = 19;
            this._buttonDelete.Text = "Удалить билет";
            this._buttonDelete.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            // 
            // _buttonAdd
            // 
            this._buttonAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this._buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonAdd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._buttonAdd.Image = ((System.Drawing.Image)(resources.GetObject("_buttonAdd.Image")));
            this._buttonAdd.Location = new System.Drawing.Point(12, 47);
            this._buttonAdd.Name = "_buttonAdd";
            this._buttonAdd.Size = new System.Drawing.Size(174, 43);
            this._buttonAdd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._buttonAdd.TabIndex = 18;
            this._buttonAdd.Text = "Добавить  билет";
            this._buttonAdd.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            // 
            // PaidTicketsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 443);
            this.Controls.Add(this._ticketsList);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "PaidTicketsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авиабилеты";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this._ticketsList)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private BrightIdeasSoftware.ObjectListView _ticketsList;
        private BrightIdeasSoftware.OLVColumn _colFio;
        private BrightIdeasSoftware.OLVColumn _colDirection;
        private BrightIdeasSoftware.OLVColumn _colReis;
        private BrightIdeasSoftware.OLVColumn _colValutTarif;
        private BrightIdeasSoftware.OLVColumn _colRate;
        private BrightIdeasSoftware.OLVColumn _colTopSbor;
        private BrightIdeasSoftware.OLVColumn _colAeroportSbor;
        private BrightIdeasSoftware.OLVColumn _colItogoPoBiletu;
        private BrightIdeasSoftware.OLVColumn _colCommission;
        private BrightIdeasSoftware.OLVColumn _colCommissionSom;
        private BrightIdeasSoftware.OLVColumn _colSkidka;
        private BrightIdeasSoftware.OLVColumn _colOplataPassajir;
        private BrightIdeasSoftware.OLVColumn _colItogo;
        private BrightIdeasSoftware.OLVColumn _colCompany;
        private BrightIdeasSoftware.OLVColumn _colCredit;
        private BrightIdeasSoftware.OLVColumn _lastNameColumn;
        private BrightIdeasSoftware.OLVColumn _firstNameColumn;
        private BrightIdeasSoftware.OLVColumn _fatherNameColumn;
        private BrightIdeasSoftware.OLVColumn _addressColumn;
        private BrightIdeasSoftware.OLVColumn _numberPhoneColumn;
        private BrightIdeasSoftware.OLVColumn _roleColumn;
        private BrightIdeasSoftware.OLVColumn _dateFirstColumn;
        private BrightIdeasSoftware.OLVColumn _loginColumn;
        private BrightIdeasSoftware.OLVColumn _passwordColumn;
        private BrightIdeasSoftware.OLVColumn _isDeleteColumn;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.ButtonX _buttonDelete;
        private DevComponents.DotNetBar.ButtonX _buttonAdd;
    }
}