﻿namespace AVIA.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this._textBoxLogin = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxPassword = new Telerik.WinControls.UI.RadTextBoxControl();
            this._buttonLogin = new DevComponents.DotNetBar.Controls.SwitchButton();
            this._buttonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // _textBoxLogin
            // 
            this._textBoxLogin.Font = new System.Drawing.Font("Times New Roman", 12F);
            this._textBoxLogin.Location = new System.Drawing.Point(375, 28);
            this._textBoxLogin.Name = "_textBoxLogin";
            this._textBoxLogin.NullText = " логин";
            this._textBoxLogin.Size = new System.Drawing.Size(135, 27);
            this._textBoxLogin.TabIndex = 1;
            this._textBoxLogin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _textBoxPassword
            // 
            this._textBoxPassword.Font = new System.Drawing.Font("Times New Roman", 12F);
            this._textBoxPassword.Location = new System.Drawing.Point(375, 79);
            this._textBoxPassword.Name = "_textBoxPassword";
            this._textBoxPassword.NullText = " пароль";
            this._textBoxPassword.Size = new System.Drawing.Size(135, 27);
            this._textBoxPassword.TabIndex = 2;
            this._textBoxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._textBoxPassword.UseSystemPasswordChar = true;
            // 
            // _buttonLogin
            // 
            // 
            // 
            // 
            this._buttonLogin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._buttonLogin.Location = new System.Drawing.Point(388, 122);
            this._buttonLogin.Name = "_buttonLogin";
            this._buttonLogin.OffBackColor = System.Drawing.Color.Tomato;
            this._buttonLogin.OffText = "Войти";
            this._buttonLogin.OnBackColor = System.Drawing.Color.Chartreuse;
            this._buttonLogin.OnText = "Вход";
            this._buttonLogin.OnTextColor = System.Drawing.Color.Green;
            this._buttonLogin.Size = new System.Drawing.Size(106, 24);
            this._buttonLogin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this._buttonLogin.TabIndex = 3;
            // 
            // _buttonClose
            // 
            this._buttonClose.Location = new System.Drawing.Point(540, 239);
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.Size = new System.Drawing.Size(26, 23);
            this._buttonClose.TabIndex = 0;
            this._buttonClose.Text = "X";
            this._buttonClose.UseVisualStyleBackColor = true;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(566, 263);
            this.Controls.Add(this._buttonClose);
            this.Controls.Add(this._buttonLogin);
            this.Controls.Add(this._textBoxPassword);
            this.Controls.Add(this._textBoxLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoginForm";
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxPassword)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBoxControl _textBoxLogin;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxPassword;
        private DevComponents.DotNetBar.Controls.SwitchButton _buttonLogin;
        private System.Windows.Forms.Button _buttonClose;
    }
}