﻿namespace AVIA.Forms
{
    partial class UsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this._inputSalary = new DevComponents.Editors.IntegerInput();
            this._buttonReset = new Telerik.WinControls.UI.RadRepeatButton();
            this._buttonSave = new Telerik.WinControls.UI.RadButton();
            this._dateFirst = new Telerik.WinControls.UI.RadDateTimePicker();
            this._dBoxRole = new Telerik.WinControls.UI.RadDropDownList();
            this._textBoxPassword = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxFatherName = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxNumberPhone = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxLogin = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxFirstName = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxAddress = new Telerik.WinControls.UI.RadTextBoxControl();
            this._textBoxLastName = new Telerik.WinControls.UI.RadTextBoxControl();
            this._usersList = new BrightIdeasSoftware.ObjectListView();
            this._lastNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._firstNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._fatherNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._addressColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._numberPhoneColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._roleColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._dateFirstColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._loginColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._passwordColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._isDeleteColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._menuUsers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonReset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dBoxRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFatherName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxNumberPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersList)).BeginInit();
            this._menuUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Silver;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.groupPanel1.Controls.Add(this.radLabel1);
            this.groupPanel1.Controls.Add(this._inputSalary);
            this.groupPanel1.Controls.Add(this._buttonReset);
            this.groupPanel1.Controls.Add(this._buttonSave);
            this.groupPanel1.Controls.Add(this._dateFirst);
            this.groupPanel1.Controls.Add(this._dBoxRole);
            this.groupPanel1.Controls.Add(this._textBoxPassword);
            this.groupPanel1.Controls.Add(this._textBoxFatherName);
            this.groupPanel1.Controls.Add(this._textBoxNumberPhone);
            this.groupPanel1.Controls.Add(this._textBoxLogin);
            this.groupPanel1.Controls.Add(this._textBoxFirstName);
            this.groupPanel1.Controls.Add(this._textBoxAddress);
            this.groupPanel1.Controls.Add(this._textBoxLastName);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel1.Font = new System.Drawing.Font("SimHei", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1017, 239);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "Детали";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.Color.White;
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(299, 54);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(52, 18);
            this.radLabel1.TabIndex = 14;
            this.radLabel1.Text = "Позиция";
            this.radLabel1.ThemeName = "Office2010Silver";
            // 
            // _inputSalary
            // 
            this._inputSalary.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this._inputSalary.BackgroundStyle.Class = "DateTimeInputBackground";
            this._inputSalary.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this._inputSalary.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this._inputSalary.ForeColor = System.Drawing.Color.Black;
            this._inputSalary.Location = new System.Drawing.Point(299, 111);
            this._inputSalary.MaxValue = 25000;
            this._inputSalary.MinValue = 100;
            this._inputSalary.Name = "_inputSalary";
            this._inputSalary.ShowUpDown = true;
            this._inputSalary.Size = new System.Drawing.Size(227, 30);
            this._inputSalary.TabIndex = 13;
            this._inputSalary.Value = 100;
            // 
            // _buttonReset
            // 
            this._buttonReset.BackColor = System.Drawing.Color.White;
            this._buttonReset.ForeColor = System.Drawing.Color.Black;
            this._buttonReset.Location = new System.Drawing.Point(814, 92);
            this._buttonReset.Name = "_buttonReset";
            this._buttonReset.Size = new System.Drawing.Size(126, 42);
            this._buttonReset.TabIndex = 12;
            this._buttonReset.Text = "Очистить";
            // 
            // _buttonSave
            // 
            this._buttonSave.BackColor = System.Drawing.Color.White;
            this._buttonSave.ForeColor = System.Drawing.Color.Black;
            this._buttonSave.Location = new System.Drawing.Point(814, 23);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(126, 40);
            this._buttonSave.TabIndex = 11;
            this._buttonSave.Text = "Сохранить";
            // 
            // _dateFirst
            // 
            this._dateFirst.BackColor = System.Drawing.Color.White;
            this._dateFirst.ForeColor = System.Drawing.Color.Black;
            this._dateFirst.Location = new System.Drawing.Point(575, 23);
            this._dateFirst.Name = "_dateFirst";
            this._dateFirst.NullText = "Дата ";
            this._dateFirst.Size = new System.Drawing.Size(164, 20);
            this._dateFirst.TabIndex = 8;
            this._dateFirst.TabStop = false;
            this._dateFirst.Text = "20 июня 2014 г.";
            this._dateFirst.Value = new System.DateTime(2014, 6, 20, 0, 14, 44, 703);
            // 
            // _dBoxRole
            // 
            this._dBoxRole.BackColor = System.Drawing.Color.Transparent;
            this._dBoxRole.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this._dBoxRole.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._dBoxRole.ForeColor = System.Drawing.Color.Black;
            radListDataItem1.Text = "Кассир";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Оператор";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Другие";
            radListDataItem3.TextWrap = true;
            this._dBoxRole.Items.Add(radListDataItem1);
            this._dBoxRole.Items.Add(radListDataItem2);
            this._dBoxRole.Items.Add(radListDataItem3);
            this._dBoxRole.Location = new System.Drawing.Point(299, 74);
            this._dBoxRole.Name = "_dBoxRole";
            this._dBoxRole.NullText = "Роль";
            this._dBoxRole.Size = new System.Drawing.Size(227, 21);
            this._dBoxRole.TabIndex = 6;
            // 
            // _textBoxPassword
            // 
            this._textBoxPassword.BackColor = System.Drawing.Color.White;
            this._textBoxPassword.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxPassword.ForeColor = System.Drawing.Color.Black;
            this._textBoxPassword.Location = new System.Drawing.Point(575, 111);
            this._textBoxPassword.Name = "_textBoxPassword";
            this._textBoxPassword.NullText = "Пароль";
            this._textBoxPassword.Size = new System.Drawing.Size(164, 25);
            this._textBoxPassword.TabIndex = 10;
            // 
            // _textBoxFatherName
            // 
            this._textBoxFatherName.BackColor = System.Drawing.Color.White;
            this._textBoxFatherName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxFatherName.ForeColor = System.Drawing.Color.Black;
            this._textBoxFatherName.Location = new System.Drawing.Point(27, 109);
            this._textBoxFatherName.Name = "_textBoxFatherName";
            this._textBoxFatherName.NullText = "Отчество";
            this._textBoxFatherName.Size = new System.Drawing.Size(227, 25);
            this._textBoxFatherName.TabIndex = 3;
            // 
            // _textBoxNumberPhone
            // 
            this._textBoxNumberPhone.BackColor = System.Drawing.Color.White;
            this._textBoxNumberPhone.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxNumberPhone.ForeColor = System.Drawing.Color.Black;
            this._textBoxNumberPhone.Location = new System.Drawing.Point(299, 23);
            this._textBoxNumberPhone.Name = "_textBoxNumberPhone";
            this._textBoxNumberPhone.NullText = "Номер телефона";
            this._textBoxNumberPhone.Size = new System.Drawing.Size(227, 25);
            this._textBoxNumberPhone.TabIndex = 5;
            // 
            // _textBoxLogin
            // 
            this._textBoxLogin.BackColor = System.Drawing.Color.White;
            this._textBoxLogin.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxLogin.ForeColor = System.Drawing.Color.Black;
            this._textBoxLogin.Location = new System.Drawing.Point(575, 63);
            this._textBoxLogin.Name = "_textBoxLogin";
            this._textBoxLogin.NullText = "Логин";
            this._textBoxLogin.Size = new System.Drawing.Size(164, 25);
            this._textBoxLogin.TabIndex = 9;
            // 
            // _textBoxFirstName
            // 
            this._textBoxFirstName.BackColor = System.Drawing.Color.White;
            this._textBoxFirstName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxFirstName.ForeColor = System.Drawing.Color.Black;
            this._textBoxFirstName.Location = new System.Drawing.Point(27, 64);
            this._textBoxFirstName.Name = "_textBoxFirstName";
            this._textBoxFirstName.NullText = "Имя";
            this._textBoxFirstName.Size = new System.Drawing.Size(227, 25);
            this._textBoxFirstName.TabIndex = 2;
            // 
            // _textBoxAddress
            // 
            this._textBoxAddress.BackColor = System.Drawing.Color.White;
            this._textBoxAddress.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxAddress.ForeColor = System.Drawing.Color.Black;
            this._textBoxAddress.Location = new System.Drawing.Point(27, 157);
            this._textBoxAddress.Name = "_textBoxAddress";
            this._textBoxAddress.NullText = "Адрес";
            this._textBoxAddress.Size = new System.Drawing.Size(499, 25);
            this._textBoxAddress.TabIndex = 4;
            // 
            // _textBoxLastName
            // 
            this._textBoxLastName.BackColor = System.Drawing.Color.White;
            this._textBoxLastName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this._textBoxLastName.ForeColor = System.Drawing.Color.Black;
            this._textBoxLastName.Location = new System.Drawing.Point(27, 23);
            this._textBoxLastName.Name = "_textBoxLastName";
            this._textBoxLastName.NullText = "Фамилия";
            this._textBoxLastName.Size = new System.Drawing.Size(227, 25);
            this._textBoxLastName.TabIndex = 1;
            // 
            // _usersList
            // 
            this._usersList.AllColumns.Add(this._lastNameColumn);
            this._usersList.AllColumns.Add(this._firstNameColumn);
            this._usersList.AllColumns.Add(this._fatherNameColumn);
            this._usersList.AllColumns.Add(this._addressColumn);
            this._usersList.AllColumns.Add(this._numberPhoneColumn);
            this._usersList.AllColumns.Add(this._roleColumn);
            this._usersList.AllColumns.Add(this._dateFirstColumn);
            this._usersList.AllColumns.Add(this._loginColumn);
            this._usersList.AllColumns.Add(this._passwordColumn);
            this._usersList.AllColumns.Add(this._isDeleteColumn);
            this._usersList.BackColor = System.Drawing.Color.White;
            this._usersList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._usersList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lastNameColumn,
            this._firstNameColumn,
            this._fatherNameColumn,
            this._addressColumn,
            this._numberPhoneColumn,
            this._roleColumn,
            this._dateFirstColumn,
            this._loginColumn,
            this._passwordColumn,
            this._isDeleteColumn});
            this._usersList.ContextMenuStrip = this._menuUsers;
            this._usersList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._usersList.ForeColor = System.Drawing.Color.Black;
            this._usersList.FullRowSelect = true;
            this._usersList.GridLines = true;
            this._usersList.Location = new System.Drawing.Point(0, 239);
            this._usersList.Name = "_usersList";
            this._usersList.ShowGroups = false;
            this._usersList.Size = new System.Drawing.Size(1017, 279);
            this._usersList.TabIndex = 1;
            this._usersList.UseCompatibleStateImageBehavior = false;
            this._usersList.View = System.Windows.Forms.View.Details;
            // 
            // _lastNameColumn
            // 
            this._lastNameColumn.AspectName = "LastName";
            this._lastNameColumn.CellPadding = null;
            this._lastNameColumn.Text = "Фамилия";
            this._lastNameColumn.Width = 142;
            // 
            // _firstNameColumn
            // 
            this._firstNameColumn.AspectName = "FirstName";
            this._firstNameColumn.CellPadding = null;
            this._firstNameColumn.Text = "Имя";
            this._firstNameColumn.Width = 115;
            // 
            // _fatherNameColumn
            // 
            this._fatherNameColumn.AspectName = "FatherName";
            this._fatherNameColumn.CellPadding = null;
            this._fatherNameColumn.Text = "Отчество";
            this._fatherNameColumn.Width = 92;
            // 
            // _addressColumn
            // 
            this._addressColumn.AspectName = "Address";
            this._addressColumn.CellPadding = null;
            this._addressColumn.Text = "Адрес";
            this._addressColumn.Width = 168;
            // 
            // _numberPhoneColumn
            // 
            this._numberPhoneColumn.AspectName = "NumberPhone";
            this._numberPhoneColumn.CellPadding = null;
            this._numberPhoneColumn.Text = "Номер тел.";
            this._numberPhoneColumn.Width = 98;
            // 
            // _roleColumn
            // 
            this._roleColumn.AspectName = "Role";
            this._roleColumn.CellPadding = null;
            this._roleColumn.Text = "Позиция";
            this._roleColumn.Width = 85;
            // 
            // _dateFirstColumn
            // 
            this._dateFirstColumn.AspectName = "DateFirst";
            this._dateFirstColumn.CellPadding = null;
            this._dateFirstColumn.Text = "Дата ";
            this._dateFirstColumn.Width = 76;
            // 
            // _loginColumn
            // 
            this._loginColumn.AspectName = "Login";
            this._loginColumn.CellPadding = null;
            this._loginColumn.Text = "Логин";
            this._loginColumn.Width = 103;
            // 
            // _passwordColumn
            // 
            this._passwordColumn.AspectName = "Password";
            this._passwordColumn.CellPadding = null;
            this._passwordColumn.Text = "Пароль";
            this._passwordColumn.Width = 73;
            // 
            // _isDeleteColumn
            // 
            this._isDeleteColumn.AspectName = "IsDelete";
            this._isDeleteColumn.CellPadding = null;
            this._isDeleteColumn.Text = "Уделение";
            this._isDeleteColumn.Width = 62;
            // 
            // _menuUsers
            // 
            this._menuUsers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem});
            this._menuUsers.Name = "_menuUsers";
            this._menuUsers.Size = new System.Drawing.Size(119, 26);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // UsersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 518);
            this.Controls.Add(this._usersList);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UsersForm";
            this.ShowIcon = false;
            this.TitleText = "<font align =\"center\" color=\"#0072BC\"> Пользователи</font>";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inputSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonReset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dBoxRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFatherName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxNumberPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersList)).EndInit();
            this._menuUsers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private Telerik.WinControls.UI.RadRepeatButton _buttonReset;
        private Telerik.WinControls.UI.RadButton _buttonSave;
        private Telerik.WinControls.UI.RadDateTimePicker _dateFirst;
        private Telerik.WinControls.UI.RadDropDownList _dBoxRole;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxPassword;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxFatherName;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxNumberPhone;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxLogin;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxFirstName;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxAddress;
        private Telerik.WinControls.UI.RadTextBoxControl _textBoxLastName;
        private BrightIdeasSoftware.ObjectListView _usersList;
        private BrightIdeasSoftware.OLVColumn _lastNameColumn;
        private BrightIdeasSoftware.OLVColumn _firstNameColumn;
        private BrightIdeasSoftware.OLVColumn _fatherNameColumn;
        private BrightIdeasSoftware.OLVColumn _addressColumn;
        private BrightIdeasSoftware.OLVColumn _numberPhoneColumn;
        private BrightIdeasSoftware.OLVColumn _roleColumn;
        private BrightIdeasSoftware.OLVColumn _dateFirstColumn;
        private BrightIdeasSoftware.OLVColumn _loginColumn;
        private BrightIdeasSoftware.OLVColumn _passwordColumn;
        private BrightIdeasSoftware.OLVColumn _isDeleteColumn;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private DevComponents.Editors.IntegerInput _inputSalary;
        private System.Windows.Forms.ContextMenuStrip _menuUsers;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
    }
}