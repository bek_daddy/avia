﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Domain.Entities;

namespace AVIA.Forms
{
    public partial class TicketDetailsForm : DevComponents.DotNetBar.Metro.MetroForm
    {

        public event EventHandler<EventArgs> ChangeTicket;

        public TicketDetailsForm()
        {
            InitializeComponent();

            _inputValuTarif.ValueChanged += ChangedValuesTicket;
            _inputRate.ValueChanged += ChangedValuesTicket;
            _inputDopCommissia.ValueChanged += ChangedValuesTicket;
            _inputItogoPoBiletu.ValueChanged += ChangedValuesTicket;
            _inputCommissia.ValueChanged += ChangedValuesTicket;
            _inputCommissiaSom.ValueChanged += ChangedValuesTicket;
            _inputSkidka.ValueChanged += ChangedValuesTicket;
            _inputOplataPassajir.ValueChanged += ChangedValuesTicket;

            _buttonSave.Click += (sender, e) =>
            {
                if (Ticket != null &&
                    MessageBox.Show("Сохранить - " + Ticket.FIO + " ?", "Внимание..", MessageBoxButtons.YesNo) == DialogResult.Yes) Close();
            };
        }

        void ChangedValuesTicket(object sender, EventArgs e)
        {
            ChangeTicket(this, EventArgs.Empty);
        }

        public PaidTicket Ticket
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxDirection.Text.Trim()) &&
                    string.IsNullOrEmpty(_textBoxFIO.Text.Trim()) &&
                    string.IsNullOrEmpty(_textBoxReis.Text.Trim()) &&
                    _inputAeroportSbor.Value < 1 &&
                    _inputItogoPoBiletu.Value < 1 &&
                    _inputItogo.Value < 1) return null;

                return new PaidTicket
                    {
                        FIO = _textBoxFIO.Text.Trim(),
                        Direction = _textBoxDirection.Text.Trim(),
                        Reis = _textBoxReis.Text.Trim(),
                        AeroportSbor = _inputAeroportSbor.Value,
                        Comission = _inputCommissia.Value,
                        ComissionSom = _inputCommissiaSom.Value,
                        Company = (Company)_comboCompany.SelectedItem,
                        Itogo = _inputItogo.Value,
                        ItogoPoBiletu = _inputItogoPoBiletu.Value,
                        OplataPassajir = _inputOplataPassajir.Value,
                        TopSbor = _inputTopSbor.Value,
                        Skidka = _inputSkidka.Value,
                        Rate = _inputRate.Value,
                        ValutTarif = _inputValuTarif.Value,
                        User = ApplicationUser.CurrentUser,
                        DopCommissia = _inputDopCommissia.Value
                    };
            }
            set
            {
                if (value == null) return;

                _inputTarifSom.Value = value.TarifSom;
                _inputItogo.Value = value.Itogo;
                _inputCommissiaSom.Value = value.ComissionSom;
                _inputItogoPoBiletu.Value = value.ItogoPoBiletu;
                _inputOplataPassajir.Value = value.OplataPassajir;
            }
        }

        public IEnumerable<Company> Companies { set { _comboCompany.DataSource = value; } }



    }
}