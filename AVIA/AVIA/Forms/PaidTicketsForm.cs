﻿using System;
using System.Collections.Generic;
using AVIA.View;
using Domain.Entities;

namespace AVIA.Forms
{
    public partial class PaidTicketsForm : DevComponents.DotNetBar.Metro.MetroForm, IViewPaidTickets
    {
        private IEnumerable<Company> _companies;

        public PaidTicketsForm()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            _buttonAdd.Click += (sender, e) =>
                {
                    var form = new TicketDetailsForm { Companies = _companies };
                    form.ChangeTicket += (sen, er) =>
                        {
                            NewPaidTicket = form.Ticket;
                            ChangedTicket(this, EventArgs.Empty);
                            form.Ticket = NewPaidTicket;
                        };
                    form.ShowDialog();
                    NewPaidTicket = form.Ticket;
                };

            _buttonDelete.Click += (sender, e) => DeletePaidTicket(this, EventArgs.Empty);
        }


        #region View
        public void SetAllPaidTickets(IEnumerable<Ticket> paidTickets)
        {
            _ticketsList.SetObjects(paidTickets);
        }
        public void SetAllCompanies(IEnumerable<Company> companies)
        {
            _companies = companies;
        }
        public PaidTicket RemovePaidTicket
        {
            get
            {
                return
                    _ticketsList.Items.Count > 0
                        ? (PaidTicket)_ticketsList.SelectedItem.RowObject
                        : null;
            }
        }
        public PaidTicket NewPaidTicket { get; set; }
        public event EventHandler<EventArgs> DeletePaidTicket;
        public event EventHandler<EventArgs> SavePaidTicket;
        public event EventHandler<EventArgs> ChangedTicket;

        #endregion
    }
}