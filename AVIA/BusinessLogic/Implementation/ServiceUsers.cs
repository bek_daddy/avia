﻿using System;
using System.Collections.Generic;
using BusinessLogic.Interface;
using DataAccessLayer.Implementation;
using DataAccessLayer.Interface;
using Domain.Entities;

namespace BusinessLogic.Implementation
{
    public class ServiceUsers : IServiceUsers
    {
        private readonly IRepositoryUsers _repositoryUsers;

        public ServiceUsers()
        {
            _repositoryUsers = new RepositoryUsers();
        }

        public void Login(string login, string password)
        {
            var user = _repositoryUsers.GetUserByLoginPassword(login, password);
            if (user != null)
                ApplicationUser.SetUser(user);
        }

        public User SaveNewUser(string lastName, string firstName, string fatherName, string address, string numberPhone, string role,
                                double salary, DateTime dateFirst, string login, string password)
        {
            var user = new User
                {
                    LastName = lastName,
                    Address = address,
                    DateFirst = dateFirst,
                    FatherName = fatherName,
                    FirstName = firstName,
                    Login = login,
                    NumberPhone = numberPhone,
                    Password = password,
                    Role = role,
                    Salary = salary
                };

            if (IsValidateForSave(user))
            {
                _repositoryUsers.SaveUser(user);
                return user;
            }
            return null;
        }

        public User SaveNewUser(User user)
        {
            if (IsValidateForSave(user))
            {
                _repositoryUsers.SaveUser(user);
                return user;
            }
            return null;
        }

        public bool IsValidateForSave(User user)
        {
            var isDuplicate = _repositoryUsers.IsNotDuplicate(user.Login);

            if (isDuplicate && 
                !string.IsNullOrEmpty(user.LastName) &&
                !string.IsNullOrEmpty(user.FirstName) &&
                !string.IsNullOrEmpty(user.Address) &&
                !string.IsNullOrEmpty(user.NumberPhone) &&
                !string.IsNullOrEmpty(user.Role) &&
                user.Salary > 0 &&
                user.DateFirst <= DateTime.Now &&
                !string.IsNullOrEmpty(user.Login) &&
                !string.IsNullOrEmpty(user.Password))
                return true;
            return false;
        }

        public bool DeleteUser(User user)
        {
            if (!ApplicationUser.CurrentUser.Equals(user) && !ApplicationUser.IsAdmin)
                return _repositoryUsers.DeleteUser(user);
            return false;
        }

        public User UpdateUser(User user)
        {
            if (ApplicationUser.IsAdmin && user.Id != 0)
            {
                _repositoryUsers.UpdateUser(user);
                return _repositoryUsers.GetUserById(user.Id);
            }
            return null;
        }

        public IEnumerable<User> GetNotDeletedUsers()
        {
            return _repositoryUsers.GetNotDeleteUsers();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _repositoryUsers.GetAllUsers();
        }

        public bool IsDuplicate(string login)
        {
            return _repositoryUsers.IsNotDuplicate(login);
        }

        public bool IsValidateForDelete(User user)
        {
            return !ApplicationUser.CurrentUser.Equals(user) && user.Role != "admin";
        }
    }
}
