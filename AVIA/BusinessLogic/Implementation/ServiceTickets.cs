﻿using System.Collections.Generic;
using BusinessLogic.Interface;
using DataAccessLayer.Implementation;
using DataAccessLayer.Interface;
using Domain.Entities;


namespace BusinessLogic.Implementation
{
    public class ServiceTickets : IServiceTickets
    {
        private readonly IRepositoryTickets _repositoryTickets;

        public ServiceTickets()
        {
            _repositoryTickets = new RepositoryTickets();
        }

        public IEnumerable<Ticket> GetNotDeletedPaidTickets(User user)
        {
            var list = _repositoryTickets.GetAllPaidTicketsByUser(user);
            foreach (var ticket in list)
                ticket.User = user;
            return list;
        }

        public IEnumerable<Ticket> GetNotDeletedRefundTickets(User user)
        {
            var list = _repositoryTickets.GetNotDeletedRefundTicketsByUser(user);
            foreach (var ticket in list)
                ticket.User = user;
            return list;
        }

        public IEnumerable<Ticket> GetAllPaidTickets(User user)
        {
            var list = _repositoryTickets.GetAllPaidTicketsByUser(user);
            foreach (var ticket in list)
                ticket.User = user;
            return list;
        }

        public IEnumerable<Ticket> GetAllRefundTickets(User user)
        {
            var list = _repositoryTickets.GetAllRefundTicketsByUser(user);
            foreach (var ticket in list)
                ticket.User = user;
            return list;
        }

        public Ticket SaveNewPaidTicket(Ticket ticket)
        {
            var tticket = (PaidTicket)ticket;

            if (tticket != null &&
                string.IsNullOrEmpty(tticket.FIO) &&
                string.IsNullOrEmpty(tticket.Direction) &&
                string.IsNullOrEmpty(tticket.Reis) &&
                (tticket.ValutTarif > 0 || tticket.TarifSom > 0) &&
                tticket.ItogoPoBiletu > 0 &&
                tticket.OplataPassajir > 0 &&
                tticket.Itogo > 0 &&
                tticket.Company != null)
            {
                ticket.User = ApplicationUser.CurrentUser;
                return _repositoryTickets.Save(ticket);
            }
            return null;
        }

        public bool DeletePaidTicket(Ticket ticket)
        {
            if (ticket != null && ticket.Id != 0)
                return _repositoryTickets.DeletePaidTicket(ticket);
            return false;
        }

        public bool DeleteRtefundTicket(Ticket ticket)
        {
            if (ticket != null && ticket.Id != 0)
                return _repositoryTickets.DeleteRefundedTicket(ticket);
            return false;
        }

        public RefundTicket RefundTicket(PaidTicket ticket)
        {
            if (_repositoryTickets.DeletePaidTicket(ticket))
            {
                var tick = _repositoryTickets.Save(ticket);
                tick.User = ticket.User;
                return (RefundTicket)tick;
            }
            return null;
        }

        public PaidTicket CalculateNewTicket(PaidTicket ticket)
        {
            if (ticket == null) return null;

            ticket.TarifSom = ticket.ValutTarif > 0 && ticket.Rate > 0
                ? ticket.Rate * ticket.ValutTarif :
                ticket.TarifSom;                                                                 // Если тариф в валюте то умнажаем на курс

            ticket.ItogoPoBiletu = ticket.TarifSom + ticket.AeroportSbor + ticket.TopSbor;       // Итого по билету = сумма тарифа в сомах + аэропорт сбор + топливный сбор

            ticket.ComissionSom = ticket.TarifSom * ticket.Comission / 100;                      // Вычисляется комиссия в сомах от тарифа в сомах

            ticket.OplataPassajir = ticket.ItogoPoBiletu - ticket.Skidka + ticket.DopCommissia; // Оплата пассажира = Итого по билету - скидка + доп. комиссия

            ticket.Itogo = ticket.OplataPassajir - ticket.ComissionSom - ticket.DopCommissia;                         // Итого = Оплата пассжира - комиссия в сомах 

            return ticket;
        }
    }
}
