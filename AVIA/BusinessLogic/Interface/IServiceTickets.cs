﻿using System.Collections.Generic;
using Domain.Entities;

namespace BusinessLogic.Interface
{
    public interface IServiceTickets
    {
        IEnumerable<Ticket> GetNotDeletedPaidTickets(User user);
        IEnumerable<Ticket> GetNotDeletedRefundTickets(User user);

        IEnumerable<Ticket> GetAllPaidTickets(User user);
        IEnumerable<Ticket> GetAllRefundTickets(User user);
        Ticket SaveNewPaidTicket(Ticket ticket);

        bool DeletePaidTicket(Ticket ticket);
        bool DeleteRtefundTicket(Ticket ticket);
        RefundTicket RefundTicket(PaidTicket ticket);

        PaidTicket CalculateNewTicket(PaidTicket ticket);
    }
}
