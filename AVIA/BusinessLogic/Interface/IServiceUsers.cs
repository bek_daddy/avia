﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace BusinessLogic.Interface
{
    public interface IServiceUsers
    {
        void Login(string login, string password);

        User SaveNewUser(string lastName, string firstName, string fatherName, string address, string numberPhone,
                         string role,
                         double salary, DateTime dateFirst, string login, string password);

        User SaveNewUser(User user);

        bool IsValidateForSave(User user);

        bool DeleteUser(User user);

        User UpdateUser(User user);

        IEnumerable<User> GetNotDeletedUsers();

        IEnumerable<User> GetAllUsers();

        bool IsDuplicate(string user);

        bool IsValidateForDelete(User user);
    }
}
