﻿namespace Domain.Entities
{
   public class Company
    {
       public int Id { get; set; }
       public string Code { get; set; }
       public string Name { get; set; }
       public string Interest { get; set; }
       public bool IsDelete { get; set; }
    }
}
