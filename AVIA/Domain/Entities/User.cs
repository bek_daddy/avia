﻿using System;

namespace Domain.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string Address { get; set; }
        public string NumberPhone { get; set; }
        public string Role { get; set; }
        public double Salary { get; set; }
        public DateTime DateFirst { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsDelete { get; set; }

        public string GetFullName
        {
            get { return LastName + " " + FirstName + " " + FatherName; }

        }
    }
}
