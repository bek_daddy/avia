﻿namespace Domain.Entities
{
    public abstract class Ticket
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        public string Direction { get; set; }
        public bool IsOne { get; set; }
        public string Reis { get; set; }
        public double ValutTarif { get; set; }
        public double Rate { get; set; }
        public double TarifSom { get; set; }
        public double TopSbor { get; set; }
        public double AeroportSbor { get; set; }
        public double ItogoPoBiletu { get; set; }
        public double Comission { get; set; }
        public double ComissionSom { get; set; }
        public double Skidka { get; set; }
        public double OplataPassajir { get; set; }
        public double Itogo { get; set; }
        public bool IsReport { get; set; }
        public bool IsDelete { get; set; }
        public bool IsCredit { get; set; }
        public Company Company { get; set; }
        public User User { get; set; }
        public double DopCommissia { get; set; }
    }
}
