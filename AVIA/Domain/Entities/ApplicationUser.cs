﻿namespace Domain.Entities
{
    public class ApplicationUser
    {
        private static User _user;

        public static void SetUser(User user)
        {
            if (_user == null)
                _user = user;
        }

        public static User CurrentUser
        {
            get { return _user; }
        }


        public static bool IsAdmin
        {
            get { return _user.Role == "admin"; }
        }
    }
}
