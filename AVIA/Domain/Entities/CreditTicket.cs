﻿namespace Domain.Entities
{
   public class CreditTicket
    {
       public int Id { get; set; }
       public int IdTicket { get; set; }
       public decimal PaidValue { get; set; }
       public decimal CreditValue { get; set; }
    }
}
